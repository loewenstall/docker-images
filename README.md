# Standard Docker Images

With each push or MR to this repo, all docker images for all three relevant versions are built and pushed to docker.imi.de.

For a MR, all images get built with the current branch as tag prefix. 
For ex., you have a MR with your changes on branch `patch` then the image for php 7.1 will be built with the tag `php:patch-base-7.1`.

This docker compose setup contains:

* PHP 7.2 FPM (With configurable versions)
* httpd webserver (fcgi proxy to PHP)
* mysql 5.7
* phpMyAdmin

## Setup

1. Include this setup into your project (into the project root, recommended).
2. Set the local document root (might be almost /public) in .env
3. Change the configuration via the `.env` file
4. Run `docker-compose up`

## Configuration

In the `.env` file you can edit any settings for your project like

* `APPLICATION_CONTEXT` for the application context (Currently only `base` is supported)
* MySQL, see [the official docs on docker hub](https://hub.docker.com/_/mysql#environment-variables)

Special options for some images are available.

### Application Config

The dockerfiles for each context are located in a single folder under the `docker/` directory.
New contexts should be based on the `base` images (use `FROM ...:base` in new docker images). 

### httpd Image

The httpd image lets you configure a few things at runtime via environment variables:

* `APACHE_USER_ID` (Default: `1000`)
* `APACHE_GROUP_ID` (Default: `1000`)
* `APACHE_DOCUMENT_ROOT` (Default: `/var/www/htdocs`)
* `APACHE_LOG_DIR` (Default: `/var/log/apache2`)
* `APACHE_LOG_LEVEL` (Default: `warn`)

## PHP Image

The php image contains these extensions:

* mysqli
* opcache
* pdo_mysql
* soap
* gd
* bcmath
* json
* iconv
* xml
* tokenizer
* xsl
* intl
* calendar
* gettext
* pcntl
* sockets
* redis
* xdebug
* zip
* imagick

By default, **all of them are disabled**.
You can enable them with the environment variable `$PHP_EXTENSIONS_ENABLE`, for example:

```
docker run -e PHP_EXTENSIONS_ENABLE='xdebug xml gd' docker.imi.de/imi/docker-images/php:latest
```

Multiple extensions need to be seperated with a whitespace.

